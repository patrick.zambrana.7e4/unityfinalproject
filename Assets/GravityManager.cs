﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityManager : MonoBehaviour
{

    float timeCount = 0f;
    Rigidbody2D[] AllRBs;

    // Start is called before the first frame update
    void Start()
    {
        AllRBs = GameObject.FindObjectsOfType<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        timeCount += Time.deltaTime;

        if (timeCount >= 5)
        {
            foreach (Rigidbody2D rb in AllRBs)
                changeGravity(rb);

            timeCount = 0f;
        }
    }

    void changeGravity(Rigidbody2D rb)
    {
        rb.gravityScale *= -1;
    }
}
