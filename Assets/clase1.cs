﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clase1 : MonoBehaviour
{

    
    public Transform PlayerTransform;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        bool playerSeen = false;

        Vector2 lookingPlayer = Vector2.right;
        const float maxRangeDotProduct = 45f / 90f;
        const float MAX_VIEW_DIST = 4f;


        Vector2 iaToPlayer = PlayerTransform.position - transform.position;

        if (iaToPlayer.magnitude >= MAX_VIEW_DIST)
        {
            iaToPlayer.Normalize();
            lookingPlayer.Normalize();

            float dot = Vector2.Dot(iaToPlayer, lookingPlayer);
            if (dot >= maxRangeDotProduct)
            {
                playerSeen = true;
            }
            else
                playerSeen = false;

            Debug.Log(dot);
        }

        if (playerSeen)
            transform.position = transform.position + new Vector3(1f, 0f, 0f) * Time.deltaTime;

        //Debug.DrawLine(transform.position, transform.position + (Vector3)lookingPlayer * 3f, )
    }
}
