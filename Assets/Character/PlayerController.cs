﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float moveHorizontal;
    private float moveVertical;
    private Vector2 currentVelocity;
    [SerializeField] private float movementSpeed = 3f;
    private Rigidbody2D characterRigidBody;
    private bool m_FacingRight = false;

    private bool isJumping = false;
    private bool alreadyJumped = false;
    [SerializeField]
    private float jumpForce = 300f;

    // Gravity changer
    private bool invertedGravity = false;

    public GameObject roof = null;
    public GameObject floor = null;

    // Animation
    public Animator animator;
    private SpriteRenderer playerSpriteRenderer;

    // Sound
    [SerializeField] private AudioSource runSoundEffect;
    [SerializeField] private AudioSource punchSoundEffect;
    [SerializeField] private AudioSource jumpSoundEffect;


    // Start is called before the first frame update
    void Start()
    {
        roof = GameObject.Find("Roof");
        floor = GameObject.Find("Floor");
        this.characterRigidBody = GetComponent<Rigidbody2D>();
        playerSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        Application.targetFrameRate = 60;
    }

    // Update is called once per frame
    void Update()
    {
        this.moveHorizontal = Input.GetAxis("Horizontal"); // X - Axis
        this.moveVertical = Input.GetAxis("Vertical"); //Y - Axis
        this.currentVelocity = this.characterRigidBody.velocity;
        float distance = 0;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!isJumping)
            {
                isJumping = true;
                alreadyJumped = false;
                animator.SetBool("isJumping", true);
                jumpSoundEffect.Play();
            }
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
            animator.SetTrigger("punch");



        if (this.moveHorizontal != 0)
        {
            animator.SetFloat("Speed", 1); // Set Animator Speed Variable to change the animation to run.
            if (!runSoundEffect.isPlaying && !isJumping)
                runSoundEffect.Play();
            // FLIP SPRITE HORIZONTAL
            if (this.moveHorizontal < 0) // To LEFT
            {
                playerSpriteRenderer.flipX = true;
                m_FacingRight = false;
            }
            else if (this.moveHorizontal > 0) // To RIGHT
            {
                playerSpriteRenderer.flipX = false;
                m_FacingRight = true;
            }
        }
        else
        {
            animator.SetFloat("Speed", 0);
            if (m_FacingRight)
                playerSpriteRenderer.flipX = false;
            else
                playerSpriteRenderer.flipX = true;
        }

        if (characterRigidBody.gravityScale < 0) // FLIP SPRITE VERTICAL
                playerSpriteRenderer.flipY = true;
        else
                playerSpriteRenderer.flipY = false;
    }

    private void FixedUpdate()
    {

         this.characterRigidBody.velocity = new
              Vector2(this.moveHorizontal * this.movementSpeed, this.currentVelocity.y);


        if (isJumping && !alreadyJumped)
        {
            this.characterRigidBody.AddForce(characterRigidBody.gravityScale * Vector2.up * jumpForce, ForceMode2D.Force);
            this.alreadyJumped = true;
        }


    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Floor") || collision.gameObject.tag.Equals("Roof") || collision.gameObject.tag.Equals("Ball"))
        {
            this.isJumping = false;
            animator.SetBool("isJumping", false);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Ball") && Input.GetKeyDown(KeyCode.LeftShift))
        {
            Rigidbody2D ball = collision.gameObject.GetComponent<Rigidbody2D>();
            punchSoundEffect.Play();

            if (m_FacingRight)
                ball.AddForce(Vector2.right * 1000f, ForceMode2D.Force);
            else
                ball.AddForce(Vector2.left * 1000f, ForceMode2D.Force);

            //Vector2 directionPunch = (collision.collider.transform.position -  transform.position).normalized;
            

            //Debug.DrawLine(transform.position, (Vector2)transform.position + directionPunch, Color.red, 1f);
            //Debug.Log(directionPunch);

        }
    }
}
