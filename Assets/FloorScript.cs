﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FloorScript : MonoBehaviour
{
    private List<Rigidbody2D> balls = new List<Rigidbody2D>();

    // Start is called before the first frame update
    void Start()
    {

        foreach (Rigidbody2D rb in GameObject.FindObjectsOfType<Rigidbody2D>())
            if (rb.gameObject.tag.Equals("Ball"))
                balls.Add(rb);
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Rigidbody2D ball in balls)
        {
            float distance = Vector2.Distance(ball.transform.position, gameObject.transform.position);

            if (distance > 50)
                balls.Remove(ball);
        }

        if (balls.ToArray().Length == 0)
            SceneManager.LoadScene("Menu");
    }
}
